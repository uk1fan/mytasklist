import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Task } from '../../Task';

@Injectable()
export class TaskService {
  constructor(private http:Http) {
    console.log('TaskService initialized...');
  }

  getTasks() {
    return this.http.get('/api/tasks')
      .map(res => res.json());
  }

  addTask(newTask: any) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/api/task', 
      JSON.stringify(newTask), {headers: headers})
      .map(res => res.json());
  }

  deleteTask(id: string) {
    return this.http.delete('/api/task/' + id)
      .map(res => res.json());
  }

  updateStatus(newTask: any) {
    console.log(newTask);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('/api/task/' + newTask._id, 
      JSON.stringify(newTask), {headers: headers})
      .map(res => res.json());    
  }
}