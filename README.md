# MEAN Tasklist Example #

This application was built while watching "MEAN App From Scratch - MongoDB, Express, Angular 2 & NodeJS" on Brad Traversy's [Youtube](https://www.youtube.com/watch?v=PFP0oXNNveg) channel.

![MyTaskList Screenshot](mytasklist.png "MyTaskList Screenshot")


## Running the application in Development ##
From the main program folder, run *nodemon* {launches the server application with API}
```
$ nodemon
```

From the client folder, run *npm start*  {launches the client application hosting AngularJS}
```
$ npm start
```

## MongoDB ##
Created an account on [mLab](https://mlab.com) which hosts MongoDB.

To connect using the mongo shell:
mongo ds111788.mlab.com:11788/uk1fan-mytasklist -u <dbuser> -p <dbpassword>

To connect using a driver via the standard MongoDB URI:
mongodb://<dbuser>:<dbpassword>@ds111788.mlab.com:11788/uk1fan-mytasklist

To connect to the database from the application, create a config folder and add a file named database.js.
The contents of database.js needs to be:
```
var dbInfo = ['dbuser','dbpassword'];
module.exports = dbInfo;
```